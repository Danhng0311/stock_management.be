﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SM_COMMON.DTOs.Request;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_Domain.Repositories
{
    public class EnterpriseRepository : BaseRepository
    {
        #region Field
        /// <summary>
        /// Khai báo field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly string _connectionString;
        private readonly SecurityUtils _securityUtils;

        #endregion

        #region Constructor
        /// <summary>
        /// Khởi tạo constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public EnterpriseRepository(IConfiguration configuration, SecurityUtils securityUtils) : base(configuration)
        {
            _securityUtils = securityUtils;

        }
        #endregion

        public async Task<enterprise> GetEnterprise()
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var accountData = _securityUtils.GetUserDetailsFromHttpContext();

                var stepDetails = new AddNewStock();
                var sql = $@"SELECT * FROM enterprise where account_id = @id";

                // để dành cho account_id
                var parameters = new DynamicParameters();
                parameters.Add("@id", accountData.id);

                var masterEnt = await connection.QueryFirstOrDefaultAsync<enterprise>(sql, parameters);

                return masterEnt;
            }
        }

        public async Task<bool> CheckIfEnterpriseExists(string taxCode, string cccd, int? type)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var exists = 0;
                if (type == 1)
                {
                    var query = @"SELECT COUNT(1) FROM enterprise 
                          WHERE CCCD = @CCCD";

                    var parameters = new
                    {
                        CCCD = cccd
                    };

                     exists = await connection.QueryFirstOrDefaultAsync<int>(query, parameters);
                } else if (type == 2)
                {

                    var query = @"SELECT COUNT(1) FROM enterprise 
                          WHERE tax_code = @TaxCode";

                    var parameters = new
                    {
                        TaxCode = taxCode,
                    };

                     exists = await connection.QueryFirstOrDefaultAsync<int>(query, parameters);
                }

                return exists > 0;
            }
        }

        public async Task<string> CreateEnterprise(enterprise enterprise)
        {
            var accountData = _securityUtils.GetUserDetailsFromHttpContext();

            if (await CheckIfEnterpriseExists(enterprise.tax_code, enterprise.CCCD, enterprise.enterprise_type))
            {
                return "Thông tin công ty đã tồn tại. Vui lòng kiểm tra lại";
            }
            using (var connection = await GetOpenConnectionAsync())
            {
                var insertQuery = @"INSERT INTO enterprise (id, enterprise_type, enterprise_name, tax_code, address, email, phone_number, legal_document, legal_representative, legal_representative_position, enterprise_field, CCCD, website, account_id, created_date, created_by)
                VALUES (@Id, @EnterpriseType, @EnterpriseName, @TaxCode, @Address, @Email, @PhoneNumber, @LegalDocument, @LegalRepresentative, @LegalRepresentativePosition, @EnterpriseField, @CCCD, @Website, @AccountId, @CreatedAt, @CreatedBy)";

                var parameters = new
                {
                    Id = Guid.NewGuid(),
                    EnterpriseType = enterprise.enterprise_type,
                    EnterpriseName = enterprise.enterprise_name,
                    TaxCode = enterprise.enterprise_type == 1 ? enterprise.CCCD : enterprise.tax_code,
                    Address = enterprise.address,
                    Email = enterprise.email,
                    PhoneNumber = enterprise.phone_number,
                    LegalDocument = enterprise.legal_document,
                    LegalRepresentative = enterprise.legal_representative,
                    LegalRepresentativePosition = enterprise.legal_representative_position,
                    EnterpriseField = enterprise.enterprise_field,
                    CCCD = enterprise.enterprise_type == 1 ? enterprise.CCCD : enterprise.tax_code,
                    Website = enterprise.website,
                    AccountId = accountData.id,
                    CreatedAt = DateTime.UtcNow,
                    CreatedBy = accountData.username
                };

                await connection.ExecuteAsync(insertQuery, parameters);
                return "Enterprise created successfully.";
            }
        }
    }
}
