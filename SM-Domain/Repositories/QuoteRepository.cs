﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SM_COMMON.DTOs.Request;
using SM_COMMON.DTOs.Response;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_Domain.Repositories
{
    public class QuoteRepository : BaseRepository
    {
        #region Field
        /// <summary>
        /// Khai báo field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly string _connectionString;
        private readonly SecurityUtils _securityUtils;
        #endregion

        #region Constructor
        /// <summary>
        /// Khởi tạo constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public QuoteRepository(IConfiguration configuration, SecurityUtils securityUtils) : base(configuration)
        {
            _securityUtils = securityUtils;

        }
        #endregion

        public async Task<PagingResponse<quote>> GetQuote(PagingRequest pagingRequest)
        {
            var totalCount = 0;
            using (var connection = await GetOpenConnectionAsync())
            {
                var whereCondition = "";
                bool isNumeric = double.TryParse(pagingRequest.search?.Trim(), out _);
                var isHaveWhere = false;
                var offset = (pagingRequest.pageIndex - 1) * pagingRequest.pageSize;  // Calculate the offset
                var pageSize = pagingRequest.pageSize;  // Calculate the page size

                var sql = $@"SELECT * from quote ";
                var countSql = @"SELECT COUNT(*) FROM quote ";

                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    var properties = typeof(stock).GetProperties();

                    var stringProperties = properties
                        .Where(p => p.PropertyType == typeof(string));

                    var numericProperties = properties
                        .Where(p => p.PropertyType == typeof(int) || p.PropertyType == typeof(int?) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(decimal?) ||
                                    p.PropertyType == typeof(double) || p.PropertyType == typeof(float) || p.PropertyType == typeof(double?) || p.PropertyType == typeof(float?));

                    var whereClauses = new List<string>();
                    if (isNumeric)
                    {
                        whereClauses.AddRange(numericProperties.Select(p => $"{p.Name} = @NumericSearch"));
                    }
                    whereClauses.AddRange(stringProperties.Select(p => $"{p.Name} LIKE @StringSearch"));

                    whereCondition = string.Join(" OR ", whereClauses);
                }

                if (!string.IsNullOrWhiteSpace(whereCondition))
                {
                    if (isHaveWhere)
                    {
                        sql += $" AND ({whereCondition})";
                        countSql += $" AND ({whereCondition})";
                    }
                    else
                    {
                        sql += $" WHERE ({whereCondition})";
                        countSql += $" WHERE ({whereCondition})";
                    }
                }

                var parameters = new DynamicParameters();
                if (double.TryParse(pagingRequest.search?.Trim(), out double numericSearch))
                {
                    parameters.Add("@NumericSearch", numericSearch);
                }
                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    parameters.Add("@StringSearch", "%" + pagingRequest.search?.Trim() + "%");
                }

                // Lấy tổng số bản ghi
                totalCount = connection.QueryFirstOrDefault<int>(countSql, parameters);

                sql += " order by created_at OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY";

                parameters.Add("@Offset", offset);
                parameters.Add("@PageSize", pageSize);

                // Danh sách bản ghi paging
                var masterList = await connection.QueryAsync<quote>(sql, parameters);

                var listIdDistinct = masterList.Select(x => x.id).Distinct();

                var resultFinal = new PagingResponse<quote>();
                resultFinal.PageData = masterList;
                resultFinal.TotalCount = totalCount;

                return resultFinal;
            }
        }

        public async Task<string> CreateQuote(QuoteRequest quote)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var accountData = _securityUtils.GetUserDetailsFromHttpContext();

                var query = @"
                INSERT INTO quote (
                    id, description, username, email, account_id, created_at, expired_date, created_by,
                    inventory_output, infrastructure, inventory_output_unit, product_category, contract_term, contract_term_unit, deadline
                )
                VALUES (
                    @Id, @Description, @Username, @Email, @AccountId, @CreatedAt, @ExpiredDate, @CreatedBy,
                    @InventoryOutput, @Infrastructure, @InventoryOutputUnit, @ProductCategory, @ContractTerm, @ContractTermUnit, @Deadline
                )";

                var parameters = new
                {
                    Id = Guid.NewGuid(),
                    Description = quote.description,
                    Username = accountData != null && accountData.username != null ? accountData.username : "Guest",
                    Email = quote.email != null ? quote.email : accountData.email,
                    AccountId = accountData != null && accountData.id != null && accountData.id != Guid.Empty ? accountData.id : null,
                    CreatedAt = DateTime.Now,
                    ExpiredDate = DateTime.Now,
                    CreatedBy = accountData != null && accountData.username != null ? accountData.username : "Guest",
                    InventoryOutput = quote.inventory_output,
                    Infrastructure = string.Join(",", quote.infrastructure), 
                    InventoryOutputUnit = quote.inventory_output_unit,
                    ProductCategory = quote.product_category,
                    ContractTerm = quote.contract_term,
                    ContractTermUnit = quote.contract_term_unit,
                    Deadline = quote.deadline
                };

                var result = await connection.ExecuteAsync(query, parameters);

                return result.ToString();
            }
        }

        public async Task<quote> GetQuoteById(Guid id)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var stepDetails = new AddNewStock();
                var sql = $@"SELECT * FROM quote where id = @id ";

                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                var res = await connection.QueryFirstOrDefaultAsync<quote>(sql, parameters);
                return res;
            }
        }
    }
}
