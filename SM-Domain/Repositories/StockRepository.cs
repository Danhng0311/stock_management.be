﻿using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SM_COMMON.DTOs;
using SM_COMMON.DTOs.Request;
using SM_COMMON.DTOs.Response;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Transactions;
using static Dapper.SqlMapper;

namespace SM_Domain.Repositories
{
    public class StockRepository : BaseRepository
    {
        #region Field
        /// <summary>
        /// Khai báo field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly string _connectionString;
        private readonly SecurityUtils _securityUtils;
        #endregion

        #region Constructor
        /// <summary>
        /// Khởi tạo constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public StockRepository(IConfiguration configuration, SecurityUtils securityUtils) : base(configuration)
        {
            _securityUtils = securityUtils;
        }
        #endregion

        public async Task<bool> AddNewStocks(AddNewStock addNewStock)
        {
            var accountData = _securityUtils.GetUserDetailsFromHttpContext();
            
            using (var connection = await GetOpenConnectionAsync())
            {
                try
                {
                    string top1StockCode = await GetLastestStockCode();

                    var listStep = new List<step>();

                    var stockData = new stock();
                    stockData.id = Guid.NewGuid();
                    stockData.status = 1;

                    var stepProperties = addNewStock.GetType()?.GetProperties()?.Where(p => p.Name.StartsWith("step"));
                    int numSteps = stepProperties.Count();

                    for (int i = 1; i <= numSteps; i++)
                    {
                        var stepData = stepProperties.ElementAt(i - 1)?.GetValue(addNewStock);
                        if (i == 1 && stepData != null)
                        {
                            var data1 = (step1)stepData;
                            data1.stock_code = top1StockCode;
                        }
                        if (stepData != null)
                        {
                            var step = new step
                            {
                                id = Guid.NewGuid(),
                                stock_id = stockData.id,
                                step_number = i,
                                step_name = $"Step {i}",
                                step_details = JsonConvert.SerializeObject(stepData)
                            };

                            listStep.Add(step);
                        }
                    }

                    if (listStep != null && listStep.Count > 0)
                    {
                        foreach (var detail in listStep)
                        {
                            if (detail.step_number == 1)
                            {
                                var step1 = JsonConvert.DeserializeObject<step1>(detail.step_details);

                                if (step1 != null)
                                {
                                    stockData.stock_code = top1StockCode;
                                    stockData.stock_type = step1.stock_type;
                                    stockData.stock_name = step1.stock_name;
                                }
                            }

                            if (detail.step_number == 2)
                            {
                                var step2 = JsonConvert.DeserializeObject<step2>(detail.step_details);

                                if (step2 != null)
                                {
                                    stockData.address_detail = step2.district + " - " + step2.city;
                                    stockData.area = step2.land_area + "m2 - " + step2.build_area + "m2";
                                    stockData.stock_capacity = step2.stock_capacity.quantity;
                                    stockData.contact = step2.contact;
                                    stockData.city = step2.city;
                                    stockData.storage_fees = JsonConvert.SerializeObject(step2.storage_fees);
                                }
                            }
                        }
                    }

                    var stockInsertQuery = @"
                    INSERT INTO [dbo].[stock] 
                    (
                        id, status, reason_cancel, stock_code, stock_type, address_detail, area, stock_capacity, contact, stock_name, city, storage_fees, account_id
                    )
                    VALUES
                    (
                        @Id, @Status, @ReasonCancel, @StockCode, @StockType, @AddressDetail, @Area, @StockCapacity, @Contact,  @StockName, @City, @StorageFees, @AccountId
                    )";

                    var stockInsertParams = new
                    {
                        Id = stockData.id,
                        AccountId = accountData.id,
                        Status = stockData.status,
                        ReasonCancel = stockData.reason_cancel,
                        StockCode = top1StockCode,
                        StockType = stockData.stock_type,
                        AddressDetail = stockData.address_detail,
                        Area = stockData.area,
                        StockCapacity = stockData.stock_capacity,
                        Contact = stockData.contact,
                        StockName = stockData.stock_name,
                        City = stockData.city,
                        StorageFees = stockData.storage_fees
                    };
                    await connection.ExecuteAsync(stockInsertQuery, stockInsertParams);

                    var sql = @"
                    INSERT INTO step (id, stock_id, step_number, step_name, step_details) 
                    VALUES (@id, @stock_id, @step_number, @step_name, @step_details)";

                    var result = await connection.ExecuteAsync(sql, listStep);

                    return result > 0;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        private async Task<string> GetLastestStockCode()
        {
            string top1StockCode = await GetTop1StockCode();
            if (string.IsNullOrEmpty(top1StockCode))
            {
                top1StockCode = "KDV-V1-0001";
            }
            else
            {
                // Extract the numeric part of the stock code
                string numericPart = new string(top1StockCode.Where(char.IsDigit).ToArray());

                if (!string.IsNullOrEmpty(numericPart))
                {
                    int lastNumber = int.Parse(numericPart);

                    // Increment the numeric part by 1
                    lastNumber++;

                    // Construct the new stock code
                    top1StockCode = $"{top1StockCode.Substring(0, top1StockCode.Length - numericPart.Length)}{lastNumber.ToString("D4")}";
                }
                else
                {
                    int top1CountStock = await GetCountStock();
                    top1StockCode = "KDV-V1K-00" + top1CountStock;
                }
            }

            return top1StockCode;
        }

        public async Task<int> GetCountStock()
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var sqlCountStock = @"select count(*) from stock";

                var top1CountStock = await connection.QueryFirstOrDefaultAsync<int>(sqlCountStock);
                return top1CountStock;
            }
        }

        public async Task<string> GetTop1StockCode()
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var sqlGetTop1Code = @"SELECT TOP 1 stock_code FROM stock ORDER BY stock_code DESC;";

                var top1StockCode = await connection.QueryFirstOrDefaultAsync<string>(sqlGetTop1Code);
                return top1StockCode;
            }
        }

        public async Task<PagingResponse<stock>> GetStock(PagingRequest pagingRequest)
        {
            var stockList = new List<stock>();
            var totalCount = 0;
            var accountData = _securityUtils.GetUserDetailsFromHttpContext();
            using (var connection = await GetOpenConnectionAsync())
            {
                var whereCondition = "";
                bool isNumeric = double.TryParse(pagingRequest.search?.Trim(), out _);
                var isHaveWhere = false;
                var offset = (pagingRequest.pageIndex - 1) * pagingRequest.pageSize;  // Calculate the offset
                var pageSize = pagingRequest.pageSize ;  // Calculate the page size

                var sql = $@"SELECT a.id, a.status, a.reason_cancel, a.stock_code, a.stock_type, a.address_detail, a.area, a.contact, a.stock_capacity, a.stock_name, a.city, a.storage_fees, a.recommend_state FROM stock a ";

                var countSql = @"SELECT COUNT(*) FROM stock ";

                if(pagingRequest.state_custom != 0)
                {
                    CustomSQLQuery(ref sql, ref countSql, ref isHaveWhere, pagingRequest.state_custom);
                }

                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    var properties = typeof(stock).GetProperties();

                    var stringProperties = properties
                        .Where(p => p.PropertyType == typeof(string));

                    var numericProperties = properties
                        .Where(p => p.PropertyType == typeof(int) || p.PropertyType == typeof(int?) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(decimal?) ||
                                    p.PropertyType == typeof(double) || p.PropertyType == typeof(float) || p.PropertyType == typeof(double?) || p.PropertyType == typeof(float?));

                    var whereClauses = new List<string>();
                    if (isNumeric)
                    {
                        whereClauses.AddRange(numericProperties.Select(p => $"{p.Name} = @NumericSearch"));
                    }
                    whereClauses.AddRange(stringProperties.Select(p => $"{p.Name} LIKE @StringSearch"));

                    whereCondition = string.Join(" OR ", whereClauses);
                }

                if (!string.IsNullOrWhiteSpace(whereCondition))
                {
                    if (isHaveWhere)
                    {
                        sql += $" AND ({whereCondition})";
                        countSql += $" AND ({whereCondition})";
                    }
                    else
                    {
                        sql += $" WHERE ({whereCondition})";
                        countSql += $" WHERE ({whereCondition})";
                        isHaveWhere = true;
                    }
                }

                if (pagingRequest.filters != null && pagingRequest.filters.Count > 0)
                {
                    var filterClauses = new List<string>();
                    foreach (var filter in pagingRequest.filters)
                    {
                        if (filter.data_field != null && filter.value != null && filter.operator_type != null)
                        {
                            var filterClause = filter.operator_type switch
                            {
                                "equal" => $"{filter.data_field} = @Filter_{filter.data_field}",
                                "greaterthan" => $"{filter.data_field} > @Filter_{filter.data_field}",
                                "lessthan" => $"{filter.data_field} < @Filter_{filter.data_field}",
                                "contains" => $"{filter.data_field} LIKE @Filter_{filter.data_field}",
                                "startsWith" => $"{filter.data_field} LIKE @Filter_{filter.data_field}",
                                "endsWith" => $"{filter.data_field} LIKE @Filter_{filter.data_field}",
                                _ => null
                            };
                            if (filterClause != null)
                            {
                                filterClauses.Add(filterClause);
                            }
                        }
                    }

                    if (filterClauses.Count > 0)
                    {
                        var filterCondition = string.Join(" AND ", filterClauses);
                        if (isHaveWhere)
                        {
                            sql += $" AND ({filterCondition})";
                            countSql += $" AND ({filterCondition})";
                        }
                        else
                        {
                            sql += $" WHERE ({filterCondition})";
                            countSql += $" WHERE ({filterCondition})";
                            isHaveWhere = true;
                        }
                    }
                }

                var parameters = new DynamicParameters();
                if (double.TryParse(pagingRequest.search?.Trim(), out double numericSearch))
                {
                    parameters.Add("@NumericSearch", numericSearch);
                }
                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    parameters.Add("@StringSearch", "%" + pagingRequest.search?.Trim() + "%");
                }

                if(pagingRequest.state_custom == 2 && accountData != null)
                {
                    parameters.Add("@accountId", accountData.id);
                }

                if (pagingRequest.filters != null)
                {
                    foreach (var filter in pagingRequest.filters)
                    {
                        if (filter.data_field != null && filter.value != null && filter.operator_type != null)
                        {
                            var parameterValue = filter.operator_type switch
                            {
                                "contains" => "%" + filter.value + "%",
                                "startsWith" => filter.value + "%",
                                "endsWith" => "%" + filter.value,
                                _ => filter.value
                            };
                            parameters.Add($"@Filter_{filter.data_field}", parameterValue);
                        }
                    }
                }

                // Lấy tổng số bản ghi
                totalCount = connection.QueryFirstOrDefault<int>(countSql, parameters);

                sql += " order by a.stock_code desc OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY";

                parameters.Add("@Offset", offset);
                parameters.Add("@PageSize", pageSize);

                // Danh sách bản ghi paging
                var masterList = await connection.QueryAsync<stock>(sql, parameters);

                var listIdDistinct = masterList.Select(x => x.id).Distinct();

                var resultFinal = new PagingResponse<stock>();
                resultFinal.PageData = masterList;
                resultFinal.TotalCount = totalCount;

                return resultFinal;
            }
        }
        public void CustomSQLQuery(ref string sql, ref string countSql, ref bool isHaveWhere, int? state)
        {
            if(state == 1)
            {
                sql = $@"SELECT a.id, a.status, a.reason_cancel, a.stock_code, a.stock_type, a.address_detail, a.area, a.contact, a.stock_capacity, a.stock_name, a.city, a.storage_fees, a.recommend_state FROM stock a where a.status = 1";

                countSql = @"SELECT COUNT(*) FROM stock where status = 1";

                isHaveWhere = true;
            }
            if (state == 2)
            {
                sql = $@"SELECT a.id, a.status, a.reason_cancel, a.stock_code, a.stock_type, a.address_detail, a.area, a.contact, a.stock_capacity, a.stock_name, a.city, a.storage_fees, a.recommend_state FROM stock a where a.account_id = @accountId";

                countSql = @"SELECT COUNT(*) FROM stock where account_id = @accountId";

                isHaveWhere = true;
            }
            if (state == 3)
            {
                sql = $@"SELECT a.id, a.status, a.reason_cancel, a.stock_code, a.stock_type, a.address_detail, a.area, a.contact, a.stock_capacity, a.stock_name, a.city, a.storage_fees, a.recommend_state FROM stock a where a.status = 2";

                countSql = @"SELECT COUNT(*) FROM stock where status = 2";

                isHaveWhere = true;
            }
        }
        public async Task<AddNewStock> GetStockById(Guid id)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var stepDetails = new AddNewStock();
                var sql = $@"SELECT * FROM step where stock_id = @id ";

                var parameters = new DynamicParameters();
                parameters.Add("@id", id);


                var listDetails = await connection.QueryAsync<step>(sql, parameters);

                if (listDetails != null && listDetails.Count() > 0) {

                    foreach (var item in listDetails)
                    {
                        if (item.step_number == 1) {
                            var step1 = JsonConvert.DeserializeObject<step1>(item.step_details);
                            stepDetails.step1 = step1;
                        }

                        if (item.step_number == 2)
                        {
                            var step2 = JsonConvert.DeserializeObject<step2>(item.step_details);
                            stepDetails.step2 = step2;
                        }

                        if (item.step_number == 3)
                        {
                            var step3 = JsonConvert.DeserializeObject<step3>(item.step_details);
                            stepDetails.step3 = step3;
                        }

                        if (item.step_number == 4)
                        {
                            var step4 = JsonConvert.DeserializeObject<step4>(item.step_details);
                            stepDetails.step4 = step4;
                        }
                    }
                }
                return stepDetails;
            }

        }


        public async Task<account> GetAccountByStockId(Guid id)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var stepDetails = new AddNewStock();
                var sql = $@"select a.* from account a where a.id = (select account_id from stock where id = @id )";

                var parameters = new DynamicParameters();
                parameters.Add("@id", id);


                var accountData = await connection.QueryFirstOrDefaultAsync<account>(sql, parameters);

                if (accountData != null)
                {
                    return accountData;
                }
                return new account();
            }

        }

        public async Task<bool> ApproveStock(ChangeStatusStock changeStatus)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                string query = @"
                UPDATE stock
                SET status = @status,
                    reason_cancel = @reason
                WHERE id = @id";

                var parameters = new
                {
                    status = changeStatus.status,
                    reason = changeStatus.reason,
                    id = changeStatus.id
                };

                var res = await connection.ExecuteAsync(query, parameters);

                return res > 0;
            }
        }
    }
}
