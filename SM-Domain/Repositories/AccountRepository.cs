﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SM_COMMON.DTOs.Request;
using SM_COMMON.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using SM_COMMON.DTOs;
using SM_COMMON.DTOs.Response;
using Microsoft.Data.SqlClient;
using System.ComponentModel;
using System.Net.NetworkInformation;
using SM_COMMON.Utils;

namespace SM_Domain.Repositories
{
    public class AccountRepository : BaseRepository
    {
        #region Field
        /// <summary>
        /// Khai báo field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly string _connectionString;
        private readonly SecurityUtils _securityUtils;

        #endregion

        #region Constructor
        /// <summary>
        /// Khởi tạo constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public AccountRepository(IConfiguration configuration, SecurityUtils securityUtils) : base(configuration)
        {
            _securityUtils =  securityUtils;
        }
        #endregion

        public static bool IsValidEmail(string email)
        {
            string pattern = @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(email);
        }

        public static bool IsValidNumber(string number)
        {
            string pattern = @"^\d+$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(number);
        }
        public async Task<bool> UpgradeAccount(UpgradeAccount accountReq)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var startDate = DateTime.Now;
                var expiredDate = DateTime.Now;
                switch (accountReq.license)
                {
                    case 1:
                        expiredDate = startDate.AddDays(90);
                        break;
                    case 2:
                        expiredDate = startDate.AddMonths(6);
                        break;
                    case 3:
                        expiredDate = startDate.AddMonths(12);
                        break;
                }
                string query = @"
                UPDATE account
                SET start_date = @StartDate,
                    expired_date = @ExpiredDate,
                    active = 3
                WHERE id = @Id";

                var parameters = new
                {
                    Id = accountReq.id,
                    StartDate = startDate,
                    ExpiredDate = expiredDate,
                };

                var res = await connection.ExecuteAsync(query, parameters);

                return res > 0;
            }
        }

        public async Task<bool> UpdateaTempStatus(int? license)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var accountData = _securityUtils.GetUserDetailsFromHttpContext();
                if(accountData == null)
                {
                    return false;
                }

                string query = @"
                UPDATE account
                SET active = @active,
                    license = @license
                WHERE id = @id";

                var parameters = new
                {
                    id = accountData.id,
                    active = 2,
                    license = license
                };

                var res = await connection.ExecuteAsync(query, parameters);

                return res > 0;
            }
        }

        public async Task<bool> Signup(account accountReq)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var checkEmail = IsValidEmail(accountReq.email);

                if(!checkEmail)
                {
                    return false;
                }

                var checkPhoneNumber = IsValidNumber(accountReq.phonenumber);

                if (!checkPhoneNumber)
                {
                    return false;
                }

                var checkEmailSQL = $@"SELECT * FROM account where email = @email";
                var parameters = new DynamicParameters();
                parameters.Add("@email", accountReq.email);

                var dataAccount = await connection.QueryAsync(checkEmailSQL, parameters);

                if(dataAccount != null && dataAccount.Count() > 0)
                {
                    return false;
                }

                var sql = $@"
                    INSERT INTO [dbo].[account]
                    ([id]
                    ,[username]
                    ,[email]
                    ,[password]
                    ,[phonenumber]
                    ,[active]
                    ,[created_date]
                    ,[created_by]
                    ,[modified_date]
                    ,[modified_by]
                    ,[role]
                    ,[deleted]
                    ,[first_name]
                    ,[last_name])
                VALUES
                    (@id
                    ,@username
                    ,@email
                    ,@password
                    ,@phonenumber
                    ,@active
                    ,@created_date
                    ,@created_by
                    ,@modified_date
                    ,@modified_by
                    ,@role
                    ,@deleted
                    ,@firstname
                    ,@lastname)
                ";

                parameters.Add("@id", Guid.NewGuid());
                parameters.Add("@username", accountReq.last_name + " " + accountReq.first_name);
                parameters.Add("@password", accountReq.password);   
                parameters.Add("@phonenumber", accountReq.phonenumber);
                parameters.Add("@active", true);
                parameters.Add("@created_date", DateTime.Now);
                parameters.Add("@created_by", accountReq.last_name + " " + accountReq.first_name);
                parameters.Add("@modified_date", DateTime.Now);
                parameters.Add("@modified_by", accountReq.last_name + " " + accountReq.first_name);
                parameters.Add("@role", accountReq.role);
                parameters.Add("@deleted", false);
                parameters.Add("@firstname", accountReq.first_name);
                parameters.Add("@lastname", accountReq.last_name);

                var rowsAffected = await connection.ExecuteAsync(sql, parameters);

                return rowsAffected > 0 ? true : false;
            }
        }

        public async Task<account> GetAccountData()
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var accountData = _securityUtils.GetUserDetailsFromHttpContext();

                var sqlGetAccount = $@"SELECT * FROM account where id = @id";
                var parameters = new DynamicParameters();
                parameters.Add("@id", accountData.id);

                var dataAccount = await connection.QueryFirstOrDefaultAsync<account>(sqlGetAccount, parameters);

                return dataAccount;
            }
        }

        public async Task<account> Signin(UserDto userDto)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var checkEmailSQL = $@"SELECT * FROM account where email = @email and password = @password";
                var parameters = new DynamicParameters();
                parameters.Add("@email", userDto.email);
                parameters.Add("@password", userDto.password);

                var dataAccount = await connection.QueryFirstOrDefaultAsync<account>(checkEmailSQL, parameters);

                if ((dataAccount == null) || (dataAccount != null && dataAccount.password != userDto.password))
                { 
                    return new account();
                }

                return dataAccount;
            }
        }

        public async Task<AdminPage> GetInfoForAdmin()
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                var query = @"
                SELECT
                    (SELECT COUNT(*) FROM account WHERE role = 1) AS Owner,
                    (SELECT COUNT(*) FROM account WHERE role = 2) AS Rent,
                    (SELECT COUNT(*) FROM stock) AS Stock";

                var counts = await connection.QueryFirstOrDefaultAsync<(int, int, int)>(query);

                var adminPage = new AdminPage
                {
                    number_stock_owner = counts.Item1,
                    number_stock_rent = counts.Item2,
                    number_of_stock = counts.Item3
                };

                return adminPage;
            }
        }

        public async Task<PagingResponse<account>> GetAccountNotApprove(PagingRequest pagingRequest)
        {
            var accountList = new List<account>();
            var totalCount = 0;
            using (var connection = await GetOpenConnectionAsync())
            {
                var whereCondition = "";
                bool isNumeric = double.TryParse(pagingRequest.search?.Trim(), out _);
                var isHaveWhere = false;
                var offset = (pagingRequest.pageIndex - 1) * pagingRequest.pageSize;  // Calculate the offset
                var pageSize = pagingRequest.pageSize;  // Calculate the page size

                var sql = $@"SELECT * from account where active = 2";
                var countSql = @"SELECT COUNT(*) FROM account where active = 2 ";
                isHaveWhere = true;

                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    var properties = typeof(stock).GetProperties();

                    var stringProperties = properties
                        .Where(p => p.PropertyType == typeof(string));

                    var numericProperties = properties
                        .Where(p => p.PropertyType == typeof(int) || p.PropertyType == typeof(int?) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(decimal?) ||
                                    p.PropertyType == typeof(double) || p.PropertyType == typeof(float) || p.PropertyType == typeof(double?) || p.PropertyType == typeof(float?));

                    var whereClauses = new List<string>();
                    if (isNumeric)
                    {
                        whereClauses.AddRange(numericProperties.Select(p => $"{p.Name} = @NumericSearch"));
                    }
                    whereClauses.AddRange(stringProperties.Select(p => $"{p.Name} LIKE @StringSearch"));

                    whereCondition = string.Join(" OR ", whereClauses);
                }

                if (!string.IsNullOrWhiteSpace(whereCondition))
                {
                    if (isHaveWhere)
                    {
                        sql += $" AND ({whereCondition})";
                        countSql += $" AND ({whereCondition})";
                    }
                    else
                    {
                        sql += $" WHERE ({whereCondition})";
                        countSql += $" WHERE ({whereCondition})";
                    }
                }

                var parameters = new DynamicParameters();
                if (double.TryParse(pagingRequest.search?.Trim(), out double numericSearch))
                {
                    parameters.Add("@NumericSearch", numericSearch);
                }
                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    parameters.Add("@StringSearch", "%" + pagingRequest.search?.Trim() + "%");
                }

                // Lấy tổng số bản ghi
                totalCount = connection.QueryFirstOrDefault<int>(countSql, parameters);

                sql += " order by created_date OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY";

                parameters.Add("@Offset", offset);
                parameters.Add("@PageSize", pageSize);

                // Danh sách bản ghi paging
                var masterList = await connection.QueryAsync<account>(sql, parameters);

                var listIdDistinct = masterList.Select(x => x.id).Distinct();

                var resultFinal = new PagingResponse<account>();
                resultFinal.PageData = masterList;
                resultFinal.TotalCount = totalCount;

                return resultFinal;
            }
        }

        public async Task<bool> CreateCustomer(CustomerRequest customerRequest)
        {
            using (var connection = await GetOpenConnectionAsync())
            {
                string sql = "";
                var checkTaxCodeSQL = $@"SELECT * FROM customer where tax_code = @taxcode";
                var parameters = new DynamicParameters();
                parameters.Add("@taxcode", customerRequest.tax_code);

                var dataCustomer = await connection.QueryFirstOrDefaultAsync<customer_sign>(checkTaxCodeSQL, parameters);

                if(dataCustomer != null && dataCustomer.id != null && dataCustomer.id != Guid.Empty)
                {
                    sql = @"UPDATE [dbo].[customer]
                       SET 
                           [customer_name] = @CustomerName,
                           [tax_code] = @TaxCode,
                           [status] = @Status,
                           [date_sign] = @DateSign
                       WHERE [id] = @Id";
                } else
                {
                    sql = @"INSERT INTO [dbo].[customer]
                           ([id], [customer_name], [tax_code], [status], [date_sign]) 
                           VALUES (@Id, @CustomerName, @TaxCode, @Status, @DateSign)";
                }

                var res = await connection.ExecuteAsync(sql, new
                {
                    Id = dataCustomer != null && dataCustomer.id != null && dataCustomer.id != Guid.Empty ? dataCustomer.id : Guid.NewGuid(),
                    CustomerName = customerRequest.customer_name,
                    TaxCode = customerRequest.tax_code,
                    Status = customerRequest.status,
                    DateSign = customerRequest.date_sign
                });

                return res > 0;
            }
        }

        public async Task<PagingResponse<customer_sign>> GetCustomer(PagingRequest pagingRequest)
        {
            var totalCount = 0;
            using (var connection = await GetOpenConnectionAsync())
            {
                var whereCondition = "";
                bool isNumeric = double.TryParse(pagingRequest.search?.Trim(), out _);
                var isHaveWhere = false;
                var offset = (pagingRequest.pageIndex - 1) * pagingRequest.pageSize;  // Calculate the offset
                var pageSize = pagingRequest.pageSize;  // Calculate the page size

                var sql = $@"select * from customer";
                var countSql = @"SELECT COUNT(*) FROM customer";

                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    var properties = typeof(stock).GetProperties();

                    var stringProperties = properties
                        .Where(p => p.PropertyType == typeof(string));

                    var numericProperties = properties
                        .Where(p => p.PropertyType == typeof(int) || p.PropertyType == typeof(int?) || p.PropertyType == typeof(decimal) || p.PropertyType == typeof(decimal?) ||
                                    p.PropertyType == typeof(double) || p.PropertyType == typeof(float) || p.PropertyType == typeof(double?) || p.PropertyType == typeof(float?));

                    var whereClauses = new List<string>();
                    if (isNumeric)
                    {
                        whereClauses.AddRange(numericProperties.Select(p => $"{p.Name} = @NumericSearch"));
                    }
                    whereClauses.AddRange(stringProperties.Select(p => $"{p.Name} LIKE @StringSearch"));

                    whereCondition = string.Join(" OR ", whereClauses);
                }

                if (!string.IsNullOrWhiteSpace(whereCondition))
                {
                    if (isHaveWhere)
                    {
                        sql += $" AND ({whereCondition})";
                        countSql += $" AND ({whereCondition})";
                    }
                    else
                    {
                        sql += $" WHERE ({whereCondition})";
                        countSql += $" WHERE ({whereCondition})";
                    }
                }

                var parameters = new DynamicParameters();
                if (double.TryParse(pagingRequest.search?.Trim(), out double numericSearch))
                {
                    parameters.Add("@NumericSearch", numericSearch);
                }
                if (!string.IsNullOrWhiteSpace(pagingRequest.search?.Trim()))
                {
                    parameters.Add("@StringSearch", "%" + pagingRequest.search?.Trim() + "%");
                }

                // Lấy tổng số bản ghi
                totalCount = connection.QueryFirstOrDefault<int>(countSql, parameters);

                sql += " order by date_sign OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY";

                parameters.Add("@Offset", offset);
                parameters.Add("@PageSize", pageSize);

                // Danh sách bản ghi paging
                var masterList = await connection.QueryAsync<customer_sign>(sql, parameters);

                var listIdDistinct = masterList.Select(x => x.id).Distinct();

                var resultFinal = new PagingResponse<customer_sign>();
                resultFinal.PageData = masterList;
                resultFinal.TotalCount = totalCount;

                return resultFinal;
            }
        }
    }
}
