﻿using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace SM_Domain.Repositories
{
    public abstract class BaseRepository
    {
        #region Field
        /// <summary>
        /// Declare the field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        protected readonly string _connectionString;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize the constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public BaseRepository(IConfiguration configuration)
        {
            _connectionString = configuration["ConnectionString"] ?? "";
        }
        #endregion

        #region Functions
        /// <summary>
        /// Connect to the database
        /// </summary>
        /// Author : NDANH (20/05/2023)
        /// <returns>DbConnection</returns>
        public virtual async Task<DbConnection> GetOpenConnectionAsync()
        {
            var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            return connection;
        }

        #endregion
    }
}
