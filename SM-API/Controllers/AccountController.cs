﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using SM_Application.Services;
using SM_COMMON.DTOs;
using SM_COMMON.DTOs.Request;
using SM_COMMON.DTOs.Response;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

[Route("api/v1/[controller]s")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly JwtSettings _jwtSettings;
    protected readonly AccountService _entService;
    private readonly SecurityUtils _securityUtils;

    public AccountController(JwtSettings jwtSettings, AccountService entService, SecurityUtils securityUtils)
    {
        _jwtSettings = jwtSettings;
        _entService = entService;
        _securityUtils = securityUtils;
    }

    [HttpPost("signup")]
    public async Task<IActionResult> Signup(account userDto)
    {
        if (userDto == null) {
            return BadRequest();
        }
        var isAccount = await _entService.Signup(userDto);

        return Ok(isAccount);
    }

    [HttpPost("upgrade_account")]
    public async Task<IActionResult> UpgradeAccount(UpgradeAccount userDto)
    {
        if (userDto == null)
        {
            return BadRequest();
        }
        var isAccount = await _entService.UpgradeAccount(userDto);
           
        return Ok(isAccount);
    }

    [HttpPost("temp_status")]
    public async Task<IActionResult> UpdateaTempStatus(int? license)
    {
        if (license == null)
        {
            return BadRequest();
        }
        var isAccount = await _entService.UpdateaTempStatus(license);

        return Ok(isAccount);
    }

    [HttpPost("signin")]
    public async Task<IActionResult> Signin(UserDto userDto)
    {
        var dataAccount = await _entService.Signin(userDto);

        if (dataAccount == null || (dataAccount != null && (dataAccount.id == null || dataAccount.id == Guid.Empty)))
        {
            return BadRequest();
        }

        var responseDataAccount = new DataAccountResponse
        {
            username = dataAccount.username,
            email = dataAccount.email,
            phonenumber = dataAccount.phonenumber,
            role = dataAccount.role,
            license = dataAccount.license,
            active = dataAccount.active,
            start_date = dataAccount.start_date,
            expired_date = dataAccount.expired_date
        };

        var token = GenerateJwtToken(dataAccount);
        return Ok(new { Token = token, AccountData = responseDataAccount });
    }

    [HttpGet("get-account-info")]
    public IActionResult GetAccountInfo()
    {
        var accountData = _securityUtils.GetUserDetailsFromHttpContext();

        return Ok(new
        {
            UserName = accountData.username,
            Email = accountData.email,
        });
    }

    [HttpGet("get_account")]
    public async Task<IActionResult> GetAccountData()
    {
        var accountData = await _entService.GetAccountData();

        return Ok(accountData);
    }

    [HttpGet("admin_page")]
    public async Task<IActionResult> GetInfoForAdmin()
    {
        var userStatistic = await _entService.GetInfoForAdmin();

        return Ok(userStatistic);

    }

    [HttpPost("get_customer")]
    public async Task<IActionResult> GetCustomer(PagingRequest pagingRequest)
    {
        var resFinal = await _entService.GetCustomer(pagingRequest);

        return Ok(resFinal);
    }

    [HttpPost("create_customer")]
    public async Task<IActionResult> CreateCustomer(CustomerRequest customerRequest)
    {
        var resFinal = await _entService.CreateCustomer(customerRequest);

        return Ok(resFinal);
    }

    [HttpPost("get_account_notapprove")]
    public async Task<IActionResult> GetAccountNotApprove(PagingRequest pagingRequest)
    {
        var resFinal = await _entService.GetAccountNotApprove(pagingRequest);

        return Ok(resFinal);
    }

    private string GenerateJwtToken(account user)
    {
        var claims = new[]
        {
            new Claim(JwtRegisteredClaimNames.Sub, user.email),
            new Claim("keyid", user.id.ToString()), 
            new Claim("username", user.username ?? ""),
            new Claim("phone_number", user.phonenumber)
        };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            issuer: _jwtSettings.Issuer,
            audience: _jwtSettings.Audience,
            claims: claims,
            expires: DateTime.Now.AddMinutes(_jwtSettings.ExpiryMinutes),
            signingCredentials: creds);

        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}


