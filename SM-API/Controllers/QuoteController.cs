﻿using Microsoft.AspNetCore.Mvc;
using SM_Application.Services;
using SM_COMMON.DTOs.Request;
using SM_COMMON.Entities;

namespace SM_API.Controllers
{
    [Route("api/v1/[controller]s")]
    [ApiController]
    public class QuoteController : Controller
    {
        protected readonly QuoteService _entService;

        public QuoteController(QuoteService entService)
        {
            _entService = entService;
        }

        [HttpPost("get_quote")]
        public async Task<IActionResult> GetQuote(PagingRequest pagingRequest)
        {
            var resFinal = await _entService.GetQuote(pagingRequest);
            return Ok(resFinal);
        }

        [HttpPost]
        public async Task<IActionResult> CreateQuote(QuoteRequest quote)
        {
            var resFinal = await _entService.CreateQuote(quote);

            return Ok(resFinal);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuoteById(Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return BadRequest();
            }
            var resFinal = await _entService.GetQuoteById(id);

            return Ok(resFinal);
        }
    }
}
