﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SM_Application.Services;
using SM_COMMON.DTOs;
using SM_COMMON.DTOs.Inside;
using SM_COMMON.DTOs.Request;
using System;

namespace SM_API.Controllers
{
    [Route("api/v1/[controller]s")]
    [ApiController]
    public class StockController : Controller
    {
        protected readonly StockService _stockService;

        public StockController(StockService stockService)
        {
            _stockService = stockService;
        }
        [HttpPost("create_stock")]
        public IActionResult AddNewStocks(AddNewStock addNewStock)
        {
            var resFinal = _stockService.AddNewStocks(addNewStock);

            return Ok(true);
        }

        [HttpPost("get_stock")]
        public async Task<IActionResult> GetStock(PagingRequest pagingRequest)
        {
            var resFinal = await _stockService.GetStock(pagingRequest);

            return Ok(resFinal);
        }

        [HttpPost("change_status_stock")]
        public async Task<IActionResult> ApproveStock(ChangeStatusStock changeStatus)
        {
            if (changeStatus == null)
            {
                return BadRequest();
            }
            var isAccount = await _stockService.ApproveStock(changeStatus);

            return Ok(isAccount);
        }

        [HttpPost("get_stock_pending")]
        public async Task<IActionResult> GetStockPending(PagingRequest pagingRequest)
        {
            var resFinal = await _stockService.GetStock(pagingRequest);

            return Ok(resFinal);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStockById(Guid id)
        {
            if(id == null || id == Guid.Empty)
            {
                return BadRequest();
            }
            var resFinal = await _stockService.GetStockById(id);

            return Ok(resFinal);
        }
        [HttpGet("account/{id}")]
        public async Task<IActionResult> GetAccountByStockId(Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return BadRequest();
            }
            var resFinal = await _stockService.GetAccountByStockId(id);

            return Ok(resFinal);
        }

        [HttpPost("upload")]
        public async Task<IActionResult> UploadFiles([FromBody] List<FileUploadDto> files)
        {
            var uploads = Path.Combine(Directory.GetCurrentDirectory(), "Files");

            if (!Directory.Exists(uploads))
            {
                Directory.CreateDirectory(uploads);
            }

            int savedCount = 0;

            foreach (var file in files)
            {
                var filePath = Path.Combine(uploads, file.name);

                // Check if the file already exists
                if (System.IO.File.Exists(filePath))
                {
                    continue; // Skip this file
                }

                var content = Convert.FromBase64String(file.content); // Decode base64

                await System.IO.File.WriteAllBytesAsync(filePath, content);
                savedCount++;
            }

            return Ok(new { count = savedCount });
        }

        [HttpPost("get_picture")]
        public async Task<IActionResult> GetImages([FromBody] List<Picture2Name>? listInfoPic)
        {
            var imageResponses = new List<object>();

            if (listInfoPic == null)
            {
                return Ok(false);
            }

            foreach (var imageRecord in listInfoPic)
            {
                if (imageRecord != null && (string.IsNullOrEmpty(imageRecord.normalName) || imageRecord.uidName == null || imageRecord.uidName == Guid.Empty))
                {
                    return Ok(false);
                }

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Files", imageRecord.normalName);
                if (System.IO.File.Exists(filePath))
                {
                    var fileBytes = await System.IO.File.ReadAllBytesAsync(filePath);
                    var fileExtension = Path.GetExtension(imageRecord.normalName).ToLower();
                    string mimeType;

                    switch (fileExtension)
                    {
                        case ".jpg":
                        case ".jpeg":
                            mimeType = "image/jpeg";
                            break;
                        case ".png":
                            mimeType = "image/png";
                            break;
                        case ".pdf":
                            mimeType = "application/pdf";
                            break;
                        default:
                            mimeType = "application/octet-stream"; // Fallback to a generic binary stream
                            break;
                    }

                    var base64File = Convert.ToBase64String(fileBytes);
                    var fileData = new
                    {
                        uid = imageRecord.uidName,
                        fileName = imageRecord.normalName,
                        imageData = $"data:{mimeType};base64,{base64File}",
                        downloadLink = fileExtension == ".pdf"
                    ? $"/api/v1/stocks/download/{imageRecord.normalName}"
                    : null
                    };

                    imageResponses.Add(fileData);
                }
            }

            return Ok(imageResponses);
        }

        [HttpGet("download/{name}")]
        public IActionResult DownloadFile(string name)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Files", name);
            if (!System.IO.File.Exists(filePath))
            {
                return NotFound();
            }

            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var mimeType = "application/pdf"; // Since we're only handling PDF downloads here

            return File(fileBytes, mimeType, name);
        }


    }
}
