﻿using Microsoft.AspNetCore.Mvc;
using SM_Application.Services;
using SM_COMMON.DTOs.Request;
using SM_COMMON.Entities;

namespace SM_API.Controllers
{
    [Route("api/v1/[controller]s")]
    [ApiController]
    public class EnterpriseController : Controller
    {
        protected readonly EnterpriseService _entService;

        public EnterpriseController(EnterpriseService entService)
        {
            _entService = entService;
        }

        [HttpGet]
        public async Task<IActionResult> GetEnterprise()
        {
            
            var resFinal = await _entService.GetEnterprise();

            return Ok(resFinal);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEnterprise(enterprise enterprise)
        {
            // lay step 1 - 2 - 5
            var resFinal = await _entService.CreateEnterprise(enterprise);

            return Ok(resFinal);
        }
    }
}
