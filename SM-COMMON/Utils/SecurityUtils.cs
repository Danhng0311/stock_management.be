﻿using Microsoft.AspNetCore.Http;
using SM_COMMON.DTOs;
using SM_COMMON.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SM_COMMON.Utils
{
    public class SecurityUtils
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SecurityUtils(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public AccountHeader GetUserDetailsFromHttpContext()
        {
            var context = _httpContextAccessor.HttpContext;
            if (context == null)
                return new AccountHeader();

            // Get the authorization header
            var authHeader = context.Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrEmpty(authHeader) || !authHeader.StartsWith("Bearer "))
                return new AccountHeader();

            // Extract the JWT token
            var token = authHeader.Substring("Bearer ".Length).Trim();

            // Split the JWT token into its parts
            var parts = token.Split('.');
            if (parts.Length != 3)
                return new AccountHeader();

            // Decode the payload part of the JWT token
            var payload = Encoding.UTF8.GetString(Base64UrlDecode(parts[1]));

            // Parse the JSON payload and extract the user details
            var payloadDict = JsonSerializer.Deserialize<Dictionary<string, object>>(payload);
            var userId = payloadDict.TryGetValue("keyid", out var userIdObj) ? userIdObj.ToString() : null;
            var username = payloadDict.TryGetValue("username", out var usernameObj) ? usernameObj.ToString() : null;
            var email = payloadDict.TryGetValue(JwtRegisteredClaimNames.Sub, out var emailObj) ? emailObj.ToString() : null;

            // Create the AccountHeader object
            return new AccountHeader
            {
                id = !string.IsNullOrEmpty(userId) ? Guid.Parse(userId) : Guid.Empty,
                username = username,
                email = email
            };
        }

        private static byte[] Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+');
            output = output.Replace('_', '/');
            switch (output.Length % 4)
            {
                case 2: output += "=="; break;
                case 3: output += "="; break;
            }
            return Convert.FromBase64String(output);
        }



    }

}
