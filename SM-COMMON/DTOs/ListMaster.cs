﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class ListMaster
    {
        public Guid? id { get; set; }
        public int? status { get; set; }
        public Guid? stock_id { get; set; }
        public string? step_name { get; set; }
        public int? step_number { get; set; }
        public string? step_details { get; set; }
        public string? reason_cancel { get; set; }

    }
}
