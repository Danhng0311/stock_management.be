﻿using SM_COMMON.DTOs.Inside;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class step1
    {
        public string? stock_name { get; set; }
        public int? stock_type { get; set; }
        public string? stock_code { get; set; }
        public bool? is_bonded { get; set; }
        public bool? is_work_dayoff { get; set; } = false;
        public string? note { get; set; }
        public List<Picture2Name>? picture { get; set; }
        public List<StockTimeWork>? stock_time_work{ get; set; }

    }
}
