﻿using SM_COMMON.DTOs.Inside;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class step4
    {
        //Đặc trưng
        /// <summary>
        /// Phương pháp bảo quản
        /// </summary>
        public List<int>? preservation_method { get; set; }
        /// <summary>
        /// An toàn và bảo mật
        /// </summary>
        public List<int>? save_security { get; set; }
        /// <summary>
        /// Thiết bị điện tử và tự động hoá phần cứng
        /// </summary>
        public List<int>? device_electronic { get; set; }
        /// <summary>
        /// Tiện nghi
        /// </summary>
        public List<int>? convenient { get; set; }
        /// <summary>
        /// Tiết kiệm năng lượng, thân thiện môi trường
        /// </summary>
        public List<int>? energy_save { get; set; }
        /// <summary>
        /// Chứng nhận
        /// </summary>
        public List<Picture2Name>? certificate { get; set; }

        // Thông tin thêm
        /// <summary>
        /// Chiều cao tính từ mặt sàn
        /// </summary>
        public float? floor_height { get; set; }
        /// <summary>
        /// trọng lượng chịu tải của mặt sàn
        /// </summary>
        public float? weight_of_floor { get; set; }
        /// <summary>
        /// Kích thước xe tối đa
        /// </summary>
        public int? max_size_car{ get; set; }
        /// <summary>
        /// kệ hàng
        /// </summary>
        public int? shelves { get; set; }
        /// <summary>
        /// số lượng cổng
        /// </summary>
        public int? number_ports { get; set; }
        /// <summary>
        /// Kiểu bố trí nhà kho
        /// </summary>
        public int? stock_layout { get; set; }
        /// <summary>
        /// Số lượng nhân viên vận hành trung bình
        /// </summary>
        public int? staff { get; set; }
    }
}
