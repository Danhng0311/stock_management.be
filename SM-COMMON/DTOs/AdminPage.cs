﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class AdminPage
    {
        public int? number_stock_owner { get; set; }
        public int? number_stock_rent { get; set; }
        public int? number_of_stock { get; set; }
    }
}
