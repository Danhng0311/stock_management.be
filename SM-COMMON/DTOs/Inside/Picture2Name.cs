﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class Picture2Name
    {
        public Guid? uidName { get; set; }
        public string? normalName { get; set; }
    }
}
