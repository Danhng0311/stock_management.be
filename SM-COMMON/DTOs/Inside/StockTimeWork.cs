﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class StockTimeWork
    {
        public int? day_enum { get; set; }
        public int? open_hour { get; set; }
        public int? open_minute{ get; set; }
        public int? close_hour { get; set; }
        public int? close_minute { get; set; }
    }
}
