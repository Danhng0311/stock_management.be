﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class operational_capacity
    {
        public float? quantity { get; set; }
        public int? unit { get; set; }
    }
}
