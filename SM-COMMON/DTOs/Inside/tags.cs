﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class tags
    {
        public int? area { get; set; }
        public int? size { get; set; }
        public int? permission { get; set; }
        public int? automation { get; set; }
    }
}
