﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class storage_fee
    {
        public int? UOM { get; set; }
        public float? price_by_day { get; set; }
        public float? price_by_month{ get; set; }
        public bool? is_active { get; set; }
    }
}
