﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class certification_details
    {
        public int? certificate_type { get; set; }
        public string? certificate_picture { get; set; }
    }
}
