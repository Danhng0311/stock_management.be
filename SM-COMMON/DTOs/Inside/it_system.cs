﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class it_system
    {
        public string? wms_system { get; set; }
        public string? erp_system { get; set; }
        public string? other_system { get; set; }
    }
}
