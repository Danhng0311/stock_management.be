﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class service_fees
    {
        public int? service_type { get; set; }
        public int? quantity { get; set; }
        public int? UoM { get; set; }
        public int? time { get; set; }
    }
}
