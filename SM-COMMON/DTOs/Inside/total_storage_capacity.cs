﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Inside
{
    public class total_storage_capacity
    {
        public int? quantity { get; set; }
        public int? UoM { get; set; }
    }
}
