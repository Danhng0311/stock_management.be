﻿using SM_COMMON.DTOs.Inside;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class step2
    {   
        // Địa điểm
        public string? city { get; set; }
        public string? district { get; set; }
        public string? province { get; set; }
        public string? address { get; set; }
        public string? contact { get; set; }

        // Sức chứa
        /// <summary>
        /// Diện tích đất
        /// </summary>
        public float? land_area { get; set; }
        /// <summary>
        /// Diện tích xây dựng kho
        /// </summary>
        public float? build_area { get; set; }
        /// <summary>
        /// sức chứa kho
        /// </summary>
        public operational_capacity? stock_capacity { get; set; }
        /// <summary>
        /// sức chiu tai cua mat san
        /// </summary>
        public operational_capacity? floor_capacity { get; set; }

        // Phí lưu trữ
        public List<storage_fee> storage_fees { get; set; }

    }
}
