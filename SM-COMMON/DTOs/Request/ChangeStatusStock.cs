﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Request
{
    public class ChangeStatusStock
    {
        public Guid? id { get; set; }
        public int? status { get; set; }
        public string? reason { get; set; }
    }
}
