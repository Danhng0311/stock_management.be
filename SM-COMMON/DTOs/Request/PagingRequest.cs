﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Request
{
    public class PagingRequest
    {
        [DefaultValue("1")]
        public int? pageIndex { get; set; }
        [DefaultValue("10")]
        public int? pageSize { get; set; }
        [DefaultValue("")]
        public string? search { get; set; }
        public int? state_custom { get; set; } = 0;
        public List<ArrayFilter>? filters { get; set; } 
    }
}
