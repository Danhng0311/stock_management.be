﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Request
{
    public class UpgradeAccount
    {
        public Guid? id { get; set; }
        public int? license { get; set; }
    }
}
