﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Request
{
    public class QuoteRequest
    {
        public string? email { get; set; }
        public string? description { get; set; }
        public int? inventory_output { get; set; }
        public List<int>? infrastructure { get; set; }
        public int? inventory_output_unit { get; set; }
        public int? product_category { get; set; }
        public int? contract_term { get; set; }
        public int? contract_term_unit { get; set; }
        public DateTime? deadline { get; set; }
    }
}
