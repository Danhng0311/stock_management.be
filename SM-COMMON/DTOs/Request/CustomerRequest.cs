﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Request
{
    public class CustomerRequest
    {
        public string? customer_name { get; set; }
        public int? status { get; set; }
        public string? tax_code { get; set; }
        public DateTime? date_sign { get; set; }
    }
}
