﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Request
{
    public class ArrayFilter
    {
        public string? data_field { get; set; }
        public string? value { get; set; }
        public string? operator_type { get; set; }
    }
}
