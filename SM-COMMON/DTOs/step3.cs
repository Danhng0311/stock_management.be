﻿using SM_COMMON.DTOs.Inside;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class step3
    {
        // Danh mục sản phẩm
        /// <summary>
        /// Sản phẩm công nghiệp
        /// </summary>
        public List<int>? product_industry { get; set; }
        /// <summary>
        /// Dược phẩm
        /// </summary>
        public List<int>? medicine { get; set; }
        /// <summary>
        /// Vật liệu xây dựng
        /// </summary>
        public List<int>? build_material { get; set; }
        /// <summary>
        /// Nguyên vật liệu công nghiệp
        /// </summary>
        public List<int>? industry_material { get; set; }
        /// <summary>
        /// Hàng bán lẻ & Thương mại điện tử
        /// </summary>
        public List<int>? retail_ecommerce { get; set; }
        /// <summary>
        /// Hàng thực phẩm đóng gói và đồ uống
        /// </summary>
        public List<int>? package_foods { get; set; }
        /// <summary>
        /// Hàng nông, lâm, thủy sản
        /// </summary>
        public List<int>? agri_fores_aqua_foods { get; set; }

        // Cơ sở hạ tầng
        public List<int>? infractructure { get; set; }
    }
}
