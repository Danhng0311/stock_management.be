﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class AccountHeader
    {
        public string? email { get; set; }

        public string? username { get; set; }

        public Guid? id { get; set; }

    }
}
