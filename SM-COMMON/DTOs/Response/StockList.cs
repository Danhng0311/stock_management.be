﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Response
{
    public class StockList
    {
        public string? stock_name { get; set; }
        public string? stock_code { get; set; }
        public int? stock_type { get; set; }
        public string? city { get; set; }
        /// <summary>
        /// Diện tích đất
        /// </summary>
        public float? land_area { get; set; }
        /// <summary>
        /// Diện tích cơ sở
        /// </summary>
        public float? build_area { get; set; }
        /// <summary>
        /// sức chứa khả dụng
        /// </summary>
        public float? stock_capacity { get; set; }
        public string? contact { get; set; }
        public string? reason_cancel { get; set; }


    }
}
