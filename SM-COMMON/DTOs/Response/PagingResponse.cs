﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Response
{
    public class PagingResponse<TEntityDTO>
    {
        public IEnumerable<TEntityDTO> PageData { get; set; }
        public int TotalCount { get; set; }
    }
}
