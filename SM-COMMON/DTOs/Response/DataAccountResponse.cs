﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs.Response
{
    public class DataAccountResponse
    {
        public string? username { get; set; }
        public string? email { get; set; }
        public string? phonenumber { get; set; }
        public int? role { get; set; }
        public int? license { get; set; }
        public int? active { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? expired_date { get; set; }
    }
}
