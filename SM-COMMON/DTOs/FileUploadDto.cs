﻿using SM_COMMON.DTOs.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.DTOs
{
    public class FileUploadDto
    {
        public string? name { get; set; }
        public string? content { get; set; }
    }
}
