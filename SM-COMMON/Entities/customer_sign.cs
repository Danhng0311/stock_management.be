﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    public class customer_sign
    {
        [Key]
        public Guid? id { get; set; }

        public string customer_name { get; set; }

        public string tax_code { get; set; }

        public string address { get; set; }

        public int? status { get; set; } = 0;

        public DateTime? date_sign { get; set; }
    }
}
