﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    public class step
    {
        public Guid id { get; set; }
        public int? step_number { get; set; }
        public string? step_name { get; set; }
        public string? step_details { get; set; }
        public Guid? stock_id { get; set; }

    }
}
