﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    public class account : auditable_entity
    {
        public Guid? id { get; set; }
        public string? first_name { get; set; }
        public string? last_name { get; set; }
        public string? username { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public string? phonenumber { get; set; }
        public int? role { get; set; }
        public int? license { get; set; }
        public int? active { get; set; }
        public bool? deleted { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? expired_date { get; set; }
    }
}
