﻿using SM_COMMON.DTOs.Inside;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    public class stock
    {
        public Guid id { get; set; }
        public int? status { get; set; }
        public int? recommend_state { get; set; } = 0;
        public string? reason_cancel { get; set; }
        public string? stock_code { get; set; }
        public string? stock_name { get; set; }
        public string? city { get; set; }
        public int? stock_type { get; set; }
        public string? address_detail { get; set; }
        /// <summary>
        /// Diện tích đất
        /// </summary>
        public string? area { get; set; }
        /// <summary>
        /// sức chứa khả dụng
        /// </summary>
        public float? stock_capacity { get; set; }
        public string? contact { get; set; }
        public string? storage_fees { get; set; }
        public Guid? account_id { get; set; }

    }
}
