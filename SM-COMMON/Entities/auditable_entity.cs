﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    public abstract class auditable_entity
    {
        public string? created_by { get; set; }
        public DateTime? created_at { get; set; }
        public string? modified_by { get; set; }
        public DateTime? modified_at { get; set; }
    }
}
