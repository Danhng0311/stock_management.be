﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    public class enterprise : auditable_entity
    {
        public Guid? id { get; set; }

        public int? enterprise_type { get; set; }

        public string? enterprise_name { get; set; }

        public string? tax_code { get; set; }

        public string? address { get; set; }

        public string? email { get; set; }

        public string? phone_number { get; set; }

        public string? legal_document { get; set; }
        public string? legal_representative { get; set; }
        public string? legal_representative_position { get; set; }
        public int? enterprise_field { get; set; }

        public string? CCCD { get; set; }
        public string? website { get; set; }

        public Guid? account_id { get; set; }
    }
}
