﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_COMMON.Entities
{
    /// <summary>
    /// Báo giá
    /// </summary>
    public class quote
    {
        public Guid id { get; set; }
        public string? description { get; set; }
        public string? username { get; set; }
        public string? email { get; set; }
        public Guid? account_id { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? expired_date { get; set; }
        public string? created_by { get; set; }
        public int? inventory_output { get; set; }
        public string? infrastructure { get; set; }
        public int? inventory_output_unit { get; set; }
        public int? product_category { get; set; }
        public int? contract_term { get; set; }
        public int? contract_term_unit { get; set; }
        public DateTime? deadline { get; set; }

    }
}
