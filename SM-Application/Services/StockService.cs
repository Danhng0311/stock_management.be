﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SM_COMMON.DTOs.Request;
using SM_COMMON.DTOs.Response;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using SM_Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_Application.Services
{
    public class StockService
    {
        #region Field
        /// <summary>
        /// Declare the field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly StockRepository _stockRepository;
        private readonly SecurityUtils _securityUtils;

        #endregion

        #region Constructor
        /// <summary>
        /// Initialize the constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public StockService(IConfiguration configuration, SecurityUtils securityUtils)
        {
            _securityUtils = securityUtils;
            _stockRepository = new StockRepository(configuration, securityUtils);
        }
        #endregion

        #region Functions
        /// <summary>
        /// Get all stocks
        /// </summary>
        /// Author: NDANH (23/05/2023)
        /// <returns>List of stocks</returns>
        //public async Task<IEnumerable<Stock>> GetAllStocksAsync()
        //{
        //    using (var connection = await _stockRepository.GetOpenConnectionAsync())
        //    {
        //        // Perform stock-related operations here
        //        // For example, execute a SQL query to retrieve all stocks
        //        var stocks = await connection.QueryAsync<Stock>("SELECT * FROM Stocks");
        //        return stocks;
        //    }
        //}

        /// <summary>
        /// Create a new stock
        /// </summary>
        /// Author: NDANH (23/05/2023)
        /// <param name="stock">Stock object to be created</param>
        /// <returns>Created stock</returns>
        public async Task<bool> AddNewStocks(AddNewStock addNewStock)
        {
            if(addNewStock == null) return false;

            _stockRepository.AddNewStocks(addNewStock);

            return true;
        }

        public async Task<PagingResponse<stock>> GetStock(PagingRequest pagingRequest)
        {
            var resultFinal = await _stockRepository.GetStock(pagingRequest);

            return resultFinal;
        }

        public async Task<AddNewStock> GetStockById(Guid id)
        {
            var resultFinal = await _stockRepository.GetStockById(id);

            return resultFinal;
        }
        public async Task<account> GetAccountByStockId(Guid id)
        {
            var resultFinal = await _stockRepository.GetAccountByStockId(id);

            return resultFinal;
        }

        public async Task<bool> ApproveStock(ChangeStatusStock changeStatusStock)
        {
            var resultFinal = await _stockRepository.ApproveStock(changeStatusStock);

            return resultFinal;
        }
        #endregion
    }
}
