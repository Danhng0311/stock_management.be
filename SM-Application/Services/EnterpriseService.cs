﻿using Microsoft.Extensions.Configuration;
using SM_COMMON.DTOs.Request;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using SM_Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_Application.Services
{
    public class EnterpriseService
    {
        #region Field
        /// <summary>
        /// Declare the field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly EnterpriseRepository _entRepository;

        private readonly SecurityUtils _securityUtils;

        #endregion

        #region Constructor
        /// <summary>
        /// Initialize the constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public EnterpriseService(IConfiguration configuration, SecurityUtils securityUtils)
        {
            _entRepository = new EnterpriseRepository(configuration, securityUtils);
        }
        #endregion

        public async Task<enterprise> GetEnterprise()
        {
            return await _entRepository.GetEnterprise();
        }

        public async Task<string> CreateEnterprise(enterprise enterprise)
        {
            return await _entRepository.CreateEnterprise(enterprise);
        }
    }
}
