﻿using Microsoft.Extensions.Configuration;
using SM_COMMON.DTOs;
using SM_COMMON.DTOs.Request;
using SM_COMMON.DTOs.Response;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using SM_Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_Application.Services
{
    public class AccountService
    {
        #region Field
        /// <summary>
        /// Declare the field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly AccountRepository _entRepository;
        private readonly SecurityUtils _securityUtils;

        #endregion

        #region Constructor
        /// <summary>
        /// Initialize the constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public AccountService(IConfiguration configuration, SecurityUtils securityUtils)
        {
            _entRepository = new AccountRepository(configuration, securityUtils);
        }
        #endregion

        public async Task<bool> Signup(account accountReq)
        {
            return await _entRepository.Signup(accountReq);
        }

        public async Task<bool> UpgradeAccount(UpgradeAccount accountReq)
        {
            return await _entRepository.UpgradeAccount(accountReq);
        }

        public async Task<bool> UpdateaTempStatus(int? license)
        {
            return await _entRepository.UpdateaTempStatus(license);
        }

        public async Task<account> Signin(UserDto userDto)
        {
            return await _entRepository.Signin(userDto);
        }

        public async Task<account> GetAccountData()
        {
            return await _entRepository.GetAccountData();
        }

        public async Task<AdminPage> GetInfoForAdmin()
        {
            return await _entRepository.GetInfoForAdmin();
        }

        public async Task<PagingResponse<customer_sign>> GetCustomer(PagingRequest pagingRequest)
        {
            return await _entRepository.GetCustomer(pagingRequest);
        }

        public async Task<bool> CreateCustomer(CustomerRequest customerRequest)
        {
            return await _entRepository.CreateCustomer(customerRequest);
        }

        public async Task<PagingResponse<account>> GetAccountNotApprove(PagingRequest pagingRequest)
        {
            var resultFinal = await _entRepository.GetAccountNotApprove(pagingRequest);

            return resultFinal;
        }
    }
}
