﻿using Microsoft.Extensions.Configuration;
using SM_COMMON.DTOs.Request;
using SM_COMMON.DTOs.Response;
using SM_COMMON.Entities;
using SM_COMMON.Utils;
using SM_Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM_Application.Services
{
    public class QuoteService
    {
        #region Field
        /// <summary>
        /// Declare the field
        /// </summary>
        /// Author: NDANH (23/05/2023)
        private readonly QuoteRepository _entRepository;
        private readonly SecurityUtils _securityUtils;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize the constructor
        /// </summary>
        /// Author: NDANH (23/05/2023)
        public QuoteService(IConfiguration configuration, SecurityUtils securityUtils)
        {
            _entRepository = new QuoteRepository(configuration, securityUtils);
        }
        #endregion

        public async Task<PagingResponse<quote>> GetQuote(PagingRequest pagingRequest)
        {
            return await _entRepository.GetQuote(pagingRequest);
        }

        public async Task<string> CreateQuote(QuoteRequest quote)
        {
            return await _entRepository.CreateQuote(quote);
        }

        public async Task<quote> GetQuoteById(Guid id)
        {
            var resultFinal = await _entRepository.GetQuoteById(id);

            return resultFinal;
        }
    }
}
